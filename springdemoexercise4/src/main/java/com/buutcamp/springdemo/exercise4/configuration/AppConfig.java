package com.buutcamp.springdemo.exercise4.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.buutcamp.springdemo.exercise4")
public class AppConfig {

}
