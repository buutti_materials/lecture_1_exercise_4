package com.buutcamp.springdemo.exercise4.mappings;

import com.buutcamp.springdemo.exercise4.objects.Person;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("mappingImpl")
@Scope("singleton")
public class ObjectsMapping{

    private List<Map<Integer,Object>> mapList;

    public ObjectsMapping() {
        mapList = new ArrayList<Map<Integer,Object>>();
    }

    public void addHashMapEntry(Object obj) {
        Map<Integer,Object> map = new HashMap<Integer, Object>();

        boolean objectHasBeenAdded = false;
        for (int i = 0; i < mapList.size(); i++) {
            if (mapList.get(i).get(0)==obj.getClass().toString()) {
                map.put(map.size(),obj);
            }
        }
        if (objectHasBeenAdded == false) {
            map.put(0,obj.getClass().toString());
            map.put(1,obj);
        }
        mapList.add(map);
    }

    public List<Object> getMappedObject(Class cs) {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < mapList.size(); i++) {
            if (mapList.get(i).get(0).toString().equals(cs.toString())) {
                for (int k = 1; k < mapList.get(i).size(); k++) {
                    list.add(mapList.get(i).get(k));
                }
            }
        }
        return list;
    }

    public Object getMappedObject(Class cs,Integer integer) {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < mapList.size(); i++) {
            if (mapList.get(i).get(0).toString().equals(cs.toString())) {
                return mapList.get(i).get(integer);
            }
        }
        return null;
    }


}
