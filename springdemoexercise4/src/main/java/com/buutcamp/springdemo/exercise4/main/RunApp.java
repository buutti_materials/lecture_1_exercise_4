package com.buutcamp.springdemo.exercise4.main;

import com.buutcamp.springdemo.exercise4.mappings.ObjectsMapping;
import com.buutcamp.springdemo.exercise4.objects.Cat;
import com.buutcamp.springdemo.exercise4.objects.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.buutcamp.springdemo.exercise4.configuration.AppConfig;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RunApp {
    public RunApp() {
        ApplicationContext appCtx = new AnnotationConfigApplicationContext(AppConfig.class);

        ObjectsMapping mappingImpl = appCtx.getBean(ObjectsMapping.class,"mappingImpl");

        mappingImpl.addHashMapEntry(new Person("Jack","theGreat","12"));
        mappingImpl.addHashMapEntry(new Cat("Chloe","12"));
        mappingImpl.addHashMapEntry(new Person("Anna","Bradshaw","23"));
        mappingImpl.addHashMapEntry(new Cat("Kittie","3"));

        List<Object> object = mappingImpl.getMappedObject(Cat.class);
        System.out.println(((Cat)object.get(0)).firstName);
        System.out.println(((Cat)object.get(1)).firstName);

        Person obj2;
        obj2 = (Person) mappingImpl.getMappedObject(Person.class,1);
        System.out.println(obj2.lastName);

        ((AnnotationConfigApplicationContext) appCtx).close();
    }
}
